﻿
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

using Android.App;
using Android.Content;
using Android.OS;
using Android.Runtime;
using Android.Views;
using Android.Widget;

namespace TikTak
{
	[Activity (Label = "Instructions",Theme = "@android:style/Theme.NoTitleBar")]			
	public class InstructionsActivity : Activity
	{
		protected override void OnCreate (Bundle bundle)
		{
			base.OnCreate (bundle);

			// Create your application here
			SetContentView (Resource.Layout.Instructions);
			Button backButton = FindViewById<Button> (Resource.Id.backButton);

			backButton.Click += (sender, eventArgs) => backToMainPage();
		}

		//All Events
		private void backToMainPage(){
			base.OnBackPressed();
		}
	}
}

