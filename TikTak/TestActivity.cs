﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

using Android.App;
using Android.Content;
using Android.OS;
using Android.Runtime;
using Android.Views;
using Android.Widget;

using Android.Media;

namespace TikTak
{
	[Activity (Label = "TestActivity")]			
	public class TestActivity : Activity
	{
		GridView gridView;
		GameDB gamedb; 
		List<ImageAudio> list;
		ImageAudio correct;
		int niveau;
		DefinitionsAdapter def;

		protected override void OnCreate (Bundle bundle)
		{
			base.OnCreate (bundle);
			niveau = 2;
			gamedb = new GameDB (niveau, this);
			SetContentView(Resource.Layout.test);

			gridView = FindViewById<GridView>(Resource.Id.List);
			gridView.NumColumns = 2;
			gridView.FastScrollEnabled = true;
			gridView.ItemClick += OnListItemClick;
			list = new List<ImageAudio> ();
			def = new DefinitionsAdapter(this, list);
			gridView.Adapter = def;
			nextRound ();
		}

		private void nextRound(){
			Random r = new Random ();
			int correctindex = r.Next (0, (niveau * 2));
			int cathegoryindex = gamedb.getRandomCathegoryIndex();
			List<ImageAudio> list1 = new List<ImageAudio> ();
			for (int i = 0; i < niveau * 2; i++) {
				ImageAudio randomia = gamedb.getRandomImageAudio (cathegoryindex);
				list1.Add (randomia);
				if (i == correctindex) {
					correct = randomia;
				}
			}
			list = list1;
			def.RefreshNewList (list);
		}

		void OnListItemClick(object sender, AdapterView.ItemClickEventArgs e)
		{
			var t = list[e.Position];
			if (list [e.Position].imgid == correct.imgid)nextRound ();
		}
	}
}

