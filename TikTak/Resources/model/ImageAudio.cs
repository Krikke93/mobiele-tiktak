﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

using Android.App;
using Android.Content;
using Android.OS;
using Android.Runtime;
using Android.Views;
using Android.Widget;

using Android.Media;

namespace TikTak
{
	public class ImageAudio {
		private Activity gameactivity;
		public int imgid;
		public MediaPlayer player;
		public int audioid;

		public ImageAudio(Activity a, int imgid, int audioid) {
			gameactivity = a;
			this.imgid = imgid;
			this.audioid = audioid;
		}

		public void setPlayer(){
			player = MediaPlayer.Create (gameactivity, audioid);
		}

		public void playSound(){
			if (player == null)
				setPlayer ();
			player.Start ();
		}

		public void stopSound() {
			if(player != null) player.Stop ();
		}

		public void resetSound() {
			stopSound ();
			setPlayer ();
		}
	}
}

