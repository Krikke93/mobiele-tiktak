﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

using Android.App;
using Android.Content;
using Android.OS;
using Android.Runtime;
using Android.Views;
using Android.Widget;

using Android.Media;

namespace TikTak
{
	[Activity (Label = "GameActivity",Theme = "@android:style/Theme.NoTitleBar")]			
	public class GameActivity : Activity
	{
		LinearLayout audioButton;
		GridView gridView;
		ImageViewAdapter adapter;
		int niveau;
		int punten;
		int rondes;
		ISharedPreferences data;

		GameDB gamedb;
		List<ImageAudio> list;
		ImageAudio correct;

		bool isNew = false;

		protected override void OnCreate (Bundle bundle)
		{
			base.OnCreate (bundle);

			data = GetSharedPreferences ("currentGame", FileCreationMode.Private);

			niveau = Intent.GetIntExtra ("niveau", 0);
			rondes = niveau * 5;
			isNew = data.GetBoolean ("isnew", true);
			if(!isNew) getSaved ();
			// Create your application here
			SetContentView (Resource.Layout.Game);

			gamedb = new GameDB (niveau, this);
			gridView = FindViewById<GridView>(Resource.Id.Grid);
			gridView.FastScrollEnabled = true;
			gridView.ItemClick += OnListItemClick;
			audioButton = FindViewById<LinearLayout> (Resource.Id.audioButton);
			audioButton.Click += (o, e) => playAudio(o, e);
			list = new List<ImageAudio> ();
			adapter = new ImageViewAdapter(this,gridView, list, niveau);
			gridView.Adapter = adapter;
			if(isNew) nextRound ();
		}
			
		public override void OnBackPressed(){
			
		}

		private void nextRound(){
			Random r = new Random ();
			int correctindex = r.Next (0, (niveau * 2));
			if (rondes > 0) {
				int cathegoryindex = gamedb.getRandomCathegoryIndex ();
				List<ImageAudio> list1 = new List<ImageAudio> ();
				for (int i = 0; i < niveau * 2; i++) {
					ImageAudio randomia = gamedb.getRandomImageAudio (cathegoryindex);
					list1.Add (randomia);
					if (i == correctindex) {
						correct = randomia;
					}
				}
				list = list1;
				adapter.RefreshNewList (list);
				correct.playSound ();

			} else {
				endGame ();
			}
		}

		private void resumeRound() {
			adapter.RefreshNewList (list);
		}

		public void OnListItemClick(object sender, AdapterView.ItemClickEventArgs e)
		{
			var t = list[e.Position];
			if (list [e.Position].imgid == correct.imgid)punten++;
			rondes--;
			correct.stopSound ();
			nextRound ();
		}

		private void playAudio(Object o, EventArgs e) {
			correct.resetSound ();
			correct.playSound ();
		}
			
		private void endGame(){
			AlertDialog.Builder builder = new AlertDialog.Builder (this);
			AlertDialog dialog = builder.Create ();
			dialog.SetTitle ("EndGame");
			dialog.SetMessage ("You have " + punten + " points");
			dialog.SetButton("Ok", (object sender, DialogClickEventArgs e) => {
				Intent intent = new Intent();
				intent.PutExtra("score",punten);
				intent.PutExtra("niveau",niveau);
				SetResult(Result.Ok,intent);
				this.Finish();
			});
			//dialog.SetCanceledOnTouchOutside (false);
			dialog.SetCancelable (false);
			dialog.Show();
		}

		protected override void OnStart(){
			base.OnStart ();
		}

		protected override void OnResume(){
			base.OnResume ();
			if (!isNew) {
				getSaved ();
				resumeRound ();
			}
		}

		protected override void OnPause(){
			base.OnPause();
			store ();
		}

		protected override void OnStop(){
			base.OnStop ();
			store ();
		}

		protected override void OnDestroy(){
			base.OnDestroy();
		}

		private void store(){
			ISharedPreferences data = GetSharedPreferences ("currentGame", FileCreationMode.Private);
			ISharedPreferencesEditor editor = data.Edit ();
			editor.PutInt ("niveau", niveau);
			editor.PutInt ("rondes", rondes);
			editor.PutInt ("punten", punten);
			for (int i = 0; i < list.Count; i++) {
				editor.PutInt ("imgid"+i, list[i].imgid);
				editor.PutInt ("audioid"+i, list[i].audioid);
			}
			editor.PutInt ("correctimgid", correct.imgid);
			editor.PutInt ("correctaudioid", correct.audioid);
			editor.PutBoolean ("isnew", false);
			editor.Commit();

			isNew = false;
		}

		private void getSaved(){
			ISharedPreferences data = GetSharedPreferences ("currentGame", FileCreationMode.Private);
			niveau = data.GetInt ("niveau", 1);
			rondes = data.GetInt ("rondes", niveau*5);
			punten = data.GetInt ("punten", 0);
			list = new List<ImageAudio> ();
			for(int i=0; i<niveau*2; i++) {
				list.Add (new ImageAudio(this, data.GetInt("imgid"+i, 0), data.GetInt("audioid"+i, 0)));
			}
			correct = new ImageAudio(this, data.GetInt("correctimgid", 0), data.GetInt("correctaudioid", 0));
		}

	}
		
}

