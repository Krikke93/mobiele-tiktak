﻿
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

using Android.App;
using Android.Content;
using Android.OS;
using Android.Runtime;
using Android.Views;
using Android.Widget;

namespace TikTak
{
	[Activity (Label = "SettingsActivity",Theme = "@android:style/Theme.NoTitleBar")]			
	public class SettingsActivity : Activity
	{
		protected override void OnCreate (Bundle bundle)
		{
			base.OnCreate (bundle);

			// Create your application here
			SetContentView (Resource.Layout.Settings);
			Button backButton = FindViewById<Button> (Resource.Id.backButton);
			Button resetButton = FindViewById<Button> (Resource.Id.resetButton);

			backButton.Click += (sender, eventArgs) => backToMainPage();
			resetButton.Click += (sender, eventArgs) => resetProgress();
		}

		//All Events
		private void backToMainPage(){
			base.OnBackPressed();
		}

		private void resetProgress() {
			ISharedPreferences data = GetSharedPreferences ("niveauscores", FileCreationMode.Private);
			ISharedPreferencesEditor editor = data.Edit ();
			editor.Clear ();
			editor.Commit ();
			data = GetSharedPreferences ("currentGame", FileCreationMode.Private);
			editor = data.Edit ();
			editor.Clear ();
			editor.Commit ();

			AlertDialog.Builder builder = new AlertDialog.Builder (this);
			AlertDialog dialog = builder.Create ();
			dialog.SetTitle ("Reset Progress");
			dialog.SetMessage ("All progress has been reset");
			dialog.SetButton("Ok", (object sender, DialogClickEventArgs e) => {
				this.Finish();
			});
			dialog.SetCancelable (false);
			dialog.Show();
		}
	}
}

